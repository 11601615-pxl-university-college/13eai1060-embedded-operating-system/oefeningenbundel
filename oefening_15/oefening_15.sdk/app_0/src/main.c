#include "main.h"

uint32_t
main(void)
{
	xil_printf("\r\n\r\n\r\nHello, World!\r\n");
    if (init_platform() < 0) {
        xil_printf("ERROR initializing platform.\r\n");
        return -1;
    }

	sys_thread_new((const char *)   "main_thrd",
				   (lwip_thread_fn) main_thread,
				   (void *)         NULL,
                   (int)            TCPIP_THREAD_STACKSIZE,
                   (int)            DEFAULT_THREAD_PRIO);

	vTaskStartScheduler();

    /* Code should never reach this point */

    return 0;
}

static void
print_ip(char *msg, ip_addr_t *ip)
{
    xil_printf("%s\r\n", msg);

    xil_printf("%d.%d.%d.%d\r\n", ip4_addr1(ip),
    		                      ip4_addr2(ip),
								  ip4_addr3(ip),
								  ip4_addr4(ip));

    return;
}

static void
print_ip_settings(ip_addr_t *ip, ip_addr_t *mask, ip_addr_t *gw)
{
    print_ip("Board IP: ", ip  );
    print_ip("Netmask : ", mask);
    print_ip("Gateway : ", gw  );

    return;
}

uint32_t
main_thread(void)
{
#if LWIP_DHCP==1
	int mscnt = 0;
#endif
	/* initialize lwIP before calling sys_thread_new */
    lwip_init();

    /* any thread using lwIP should be created using sys_thread_new */
    xil_printf("Starting thread \"NW_THRD\"\r\n");
    sys_thread_new("NW_THRD", network_thread, NULL,
            THREAD_STACKSIZE,
            DEFAULT_THREAD_PRIO);
#if LWIP_DHCP==1
    while (1) {
#ifdef OS_IS_FREERTOS
    	vTaskDelay(DHCP_FINE_TIMER_MSECS / portTICK_RATE_MS);
#else
    	sleep(DHCP_FINE_TIMER_MSECS);
#endif
		if (server_netif.ip_addr.addr) {
			xil_printf("DHCP request success\r\n");
			print_ip_settings(&(server_netif.ip_addr), &(server_netif.netmask), &(server_netif.gw));
			/* print all application headers */
			print_headers();
			/* now we can start application threads */
			launch_app_threads();
			break;
		}
		mscnt += DHCP_FINE_TIMER_MSECS;
		if (mscnt >= 10000) {
			xil_printf("ERROR: DHCP request timed out\r\n");
			xil_printf("Configuring default IP of 192.168.1.10\r\n");
			IP4_ADDR(&(server_netif.ip_addr),  192, 168,   1, 10);
			IP4_ADDR(&(server_netif.netmask), 255, 255, 255,  0);
			IP4_ADDR(&(server_netif.gw),      192, 168,   1,  1);
			print_ip_settings(&(server_netif.ip_addr), &(server_netif.netmask), &(server_netif.gw));
			/* print all application headers */
			print_headers();
			launch_app_threads();
			break;
		}

	}
#ifdef OS_IS_FREERTOS
	vTaskDelete(NULL);
#endif
#endif

    return 0;
}
