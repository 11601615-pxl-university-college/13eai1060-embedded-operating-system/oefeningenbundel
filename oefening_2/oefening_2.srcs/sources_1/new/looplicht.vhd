library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity looplicht is
    port (
        clk : in  std_logic;
        led : out std_logic_vector(3 downto 0);
        sw  : in  std_logic
    );
end looplicht;

architecture Behavioral of looplicht is

signal prscaler : signed (27 downto 0) := "0111011100110101100101000000";
signal prscaler_counter : signed (27 downto 0) :=(others=>'0');
signal HzClock  : std_logic := '0';
signal RunLed   : signed(3 downto 0) :="0001";

---- 125 MHz clock on Zybo Board
---- prscaler => 0111011100110101100101000000 = 125000000

begin

process(clk)
begin

if rising_edge(clk) then
    prscaler_counter <= prscaler_counter + 1;
    
    if(prscaler_counter > prscaler) then
        HzClock <= not HzClock;
        prscaler_counter <= (others => '0');

        if (sw = '1') then
            RunLed <= RunLed rol 1;
        else
            RunLed <= RunLed ror 1;
        end if;
    end if;
end if;

end process;

led <= std_logic_vector(RunLed);

end Behavioral;
