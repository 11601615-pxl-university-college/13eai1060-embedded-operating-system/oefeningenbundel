library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity seg is
port ( 
        hex : in  std_logic_vector(31 downto 0);
        led : out std_logic_vector(6 downto 0)
);
end seg;

architecture Behavioral of seg is

signal hex_select : std_logic_vector(3 downto 0);

begin

hex_select <= hex(3 downto 0);

--HEX-to-seven-segment decoder
--   HEX:   in    STD_LOGIC_VECTOR (3 downto 0);
--   LED:   out   STD_LOGIC_VECTOR (6 downto 0);
--
-- segment encoinputg
--      0
--     ---
--  5 |   | 1
--     ---   <- 6
--  4 |   | 2
--     ---  .
--      3

with hex_select select
    led <= NOT("1111001") when "0001",   --1
           NOT("0100100") when "0010",   --2
           NOT("0110000") when "0011",   --3
           NOT("0011001") when "0100",   --4
           NOT("0010010") when "0101",   --5
           NOT("0000010") when "0110",   --6
           NOT("1111000") when "0111",   --7
           NOT("0000000") when "1000",   --8
           NOT("0010000") when "1001",   --9
           NOT("0001000") when "1010",   --A
           NOT("0000011") when "1011",   --b
           NOT("1000110") when "1100",   --C
           NOT("0100001") when "1101",   --d
           NOT("0000110") when "1110",   --E
           NOT("0001110") when "1111",   --F
           NOT("1000000") when others;   --0	

end Behavioral;
