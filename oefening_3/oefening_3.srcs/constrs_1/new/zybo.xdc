
set_property IOSTANDARD LVCMOS33 [get_ports {led_0[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_0[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_0[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_0[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_0[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_0[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_0[0]}]
set_property PACKAGE_PIN U20 [get_ports {led_0[1]}]
set_property PACKAGE_PIN V20 [get_ports {led_0[2]}]
set_property PACKAGE_PIN W20 [get_ports {led_0[3]}]
set_property PACKAGE_PIN Y18 [get_ports {led_0[4]}]
set_property PACKAGE_PIN Y19 [get_ports {led_0[5]}]
set_property PACKAGE_PIN T20 [get_ports {led_0[0]}]
set_property PACKAGE_PIN W18 [get_ports {led_0[6]}]
