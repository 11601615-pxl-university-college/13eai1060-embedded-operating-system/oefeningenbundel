/*
    FreeRTOS V8.2.1 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    1 tab == 4 spaces!

    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?".  Have you defined configASSERT()?  *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *   Investing in training allows your team to be as productive as       *
     *   possible as early as possible, lowering your overall development    *
     *   cost, and enabling you to bring a more robust product to market     *
     *   earlier than would otherwise be possible.  Richard Barry is both    *
     *   the architect and key author of FreeRTOS, and so also the world's   *
     *   leading authority on what is the world's most popular real time     *
     *   kernel for deeply embedded MCU designs.  Obtaining your training    *
     *   from Richard ensures your team will gain directly from his in-depth *
     *   product knowledge and years of usage experience.  Contact Real Time *
     *   Engineers Ltd to enquire about the FreeRTOS Masterclass, presented  *
     *   by Richard Barry:  http://www.FreeRTOS.org/contact
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *    You are receiving this top quality software for free.  Please play *
     *    fair and reciprocate by reporting any suspected issues and         *
     *    participating in the community forum:                              *
     *    http://www.FreeRTOS.org/support                                    *
     *                                                                       *
     *    Thank you!                                                         *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org - Documentation, books, training, latest versions,
    license and Real Time Engineers Ltd. contact details.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
/* Xilinx includes. */
#include "xil_printf.h"
#include "xparameters.h"
#include "xgpio.h"

#define SWS_CHANNEL 1
#define LED_CHANNEL 2

#define TIMER_ID	1
#define DELAY_60_SECONDS	60000UL
#define DELAY_1_SECOND		1000UL
#define EVER                ;;
#define XGPIO_INPUT         0xFFFFFFFF
#define XGPIO_OUTPUT        ~XGPIO_INPUT
/*-----------------------------------------------------------*/

/* The Tx and Rx tasks as described at the top of this file. */
static void readSwichesTask(void *pvParameters);
static void burnLedsTask(void *pvparameters);
static void vTimerCallback(TimerHandle_t pxTimer);
/*-----------------------------------------------------------*/

/* The queue used by the Tx and Rx tasks, as described at the top of this
file. */
static TaskHandle_t switchesTask;
static TaskHandle_t ledsTask;
static QueueHandle_t xQueue = NULL;
static TimerHandle_t xTimer = NULL;
char HWstring[15] = "Hello World";
long RxtaskCntr = 0;

XGpio GPIO;

int
main(void)
{
	const TickType_t x60seconds = pdMS_TO_TICKS(DELAY_60_SECONDS);
	int err;

	err = XGpio_Initialize(&GPIO, XPAR_AXI_GPIO_0_BASEADDR);
	if (err != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	XGpio_SetDataDirection(&GPIO, LED_CHANNEL, XGPIO_OUTPUT);
	XGpio_SetDataDirection(&GPIO, SWS_CHANNEL, XGPIO_INPUT);

	xil_printf("Hello from FreeRTOS example main\r\n");

	xTaskCreate(readSwichesTask,
				(const char *) "read_buttons_task",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY,
				&switchesTask);

	xTaskCreate(burnLedsTask,
				(const char *) "burn_LEDs_task",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY + 1,
				&ledsTask);

	xQueue = xQueueCreate(1, sizeof(u32));

	/* Check the queue was created. */
	configASSERT(xQueue);

	xTimer = xTimerCreate((const char *) "Timer",
						  x60seconds,
						  pdFALSE,
						  (void *) TIMER_ID,
						  vTimerCallback);
	/* Check the timer was created. */
	configASSERT(xTimer);

	xTimerStart(xTimer, 0);

	vTaskStartScheduler();

	/* Should never get here! */
	for(EVER);
}

/*-----------------------------------------------------------*/
static void
vTimerCallback( TimerHandle_t pxTimer )
{
	long lTimerId;
	configASSERT(pxTimer);

	lTimerId = (long)pvTimerGetTimerID(pxTimer);

	if (lTimerId != TIMER_ID) {
		xil_printf("FreeRTOS Hello World Example FAILED");
	}

	xil_printf("\r\n/*-----------------------------------------------------------*/\r\n");
	xil_printf("Time's up! 60 seconds have passed!");
	xil_printf("\r\n/*-----------------------------------------------------------*/\r\n");

	vTaskDelete(switchesTask);
	vTaskDelete(ledsTask);
}

/*-----------------------------------------------------------*/
static void
readSwichesTask(void *pvParameters)
{
	//const TickType_t x1second = pdMS_TO_TICKS(DELAY_1_SECOND);
	u32 buf = 0;

	for (EVER)
	{
		//vTaskDelay(x1second);

		buf = XGpio_DiscreteRead(&GPIO, SWS_CHANNEL);
		xil_printf("readSwitchesTask; buf = 0x%X\r\n", buf);

		xQueueSend(xQueue,
				   &buf,
				   0UL );
	}

	return;
}

/*-----------------------------------------------------------*/
static void
burnLedsTask(void *pvparameters)
{
	u32 buf = 0;

	for (EVER)
	{
		xQueueReceive(xQueue,
					  &buf,
					  portMAX_DELAY);

		XGpio_DiscreteWrite(&GPIO, LED_CHANNEL, buf);
		xil_printf( "burnLeds task received string from swsRead task: 0x%X\r\n", buf );
	}

	return;
}

